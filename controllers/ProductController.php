<?php



/**
 *
 */
require('models/Product.php');

class ProductController
{

    function __construct()
    {
        # code...
    }

    public function index()
    {
        $products = Product::all();
        require('views/product/index.php');
    }

    public function create()
    {
        require('views/product/create.php');
    }

    public function store()
    {
        // Crea un objeto de la clase Product.
        $product = new Product;
        // Vuelca en él el contenido del formulario.
        $product->id = $_POST['id'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = DateTime::createFromFormat('d-m-Y', $_POST['fecha']);
        $product->id_tipo = $_POST['id_tipo'];

        $product->store();
        header('location:index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        include 'views/product/edit.php';
    }

    public function update($id)
    {
        //buscar registro
        $product = Product::find($id);
        //actualizar campos
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        //ejecutar UPDATE
        $product->save();
        //redireccionar
        header('location:../index');
    }

    public function remember($id)
    {
        // echo "$id<br>";
        $product = Product::find($id);
        // var_dump($book);
        // exit();
        $_SESSION['product'] = $product->nombre;
        header('Location: /product');
    }
}
