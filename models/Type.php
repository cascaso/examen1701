<?php

/**
*
*/
require_once('app/Model.php');
require_once('Product.php');

class Type extends Model
{
    public $id;
    public $nombre;

    function __construct()
    {
        # code...
    }

    public function find($id)
    {
        $db = Type::connect();
        // echo "SELECT ...";
        $sql = "SELECT * FROM producto WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');
        return $stmt->fetch();
    }


    public function products()
    {
        //Si no existe el atributo cathegory buscar en BBDD
        if (!isset($this->products)) {
        //TODO: implementar método fromCathegory
            $this->products = Product::fromType($this->id_tipo);
        }
            //pegar el atributo al modelo

        //devolver el atributo del modelo book ($this-> ...)
        return $this->books;
    }
    public static function all()
    {
        $db = Cathegory::connect();

        $stmt = $db->prepare("SELECT * FROM tipo");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');

        $results = $stmt->fetchAll();
        return $results;
    }

    public function __get($nombre)
    {
        return $this->$nombre();
    }
}
